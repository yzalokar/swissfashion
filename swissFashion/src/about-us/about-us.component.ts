import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  showYannick(){
    var y = <HTMLCanvasElement> document.getElementById('yannick');
    var ctx = y.getContext("2d");
    ctx.font = "30px Roboto";
    ctx.fillStyle = "white"
    ctx.fillText("Air Jordan 1 Chicago",0,50);
  }

  showJoel(){
    var j = <HTMLCanvasElement> document.getElementById('joel');
    var ctx = j.getContext("2d");
    ctx.font = "30px Roboto";
    ctx.fillStyle = "white";
    ctx.fillText("Air Force 1 Low White",0,50);
  }

}
