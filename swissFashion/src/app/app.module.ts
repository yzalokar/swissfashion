import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from '../home/home.component';
import { AboutUsComponent } from '../about-us/about-us.component';
import { BlogComponent } from '../blog/blog.component';
import { TrendsComponent } from '../trends/trends.component';
import { ImpressumComponent } from '../impressum/impressum.component';

@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      AboutUsComponent,
      BlogComponent,
      TrendsComponent,
      ImpressumComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
