import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { BlogComponent } from '../blog/blog.component';
import { AboutUsComponent } from '../about-us/about-us.component';
import { TrendsComponent } from '../trends/trends.component';
import { ImpressumComponent } from '../impressum/impressum.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'impressum', component: ImpressumComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'aboutus', component: AboutUsComponent },
  { path: 'trends', component: TrendsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
